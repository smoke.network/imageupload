require('dotenv').config();
let express = require('express');
let bodyParser = require('body-parser');
let sha1 = require('node-sha1');
const fs = require('fs');
// let mkdirp = require('mkdirp');

let app = express();

const port = process.env.PORT || 7070;        // set our port

writeFile = (filepath, data) => {
    return new Promise(function(resolve, reject) {
        fs.writeFile(filepath, data, function(err) {
            if (err) reject(err);
            else resolve('success');
        });
    });
};

// makeDir = (path) => {
//     return new Promise(function(resolve, reject) {
//         mkdirp(path, function (err) {
//             if (err) reject(err);
//             else resolve('success');
//         });
//     });
// };

serverStart = () => {
    app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));
    app.use(bodyParser.json({ type: 'application/json', limit: '10mb'}));

    let router = express.Router();
    router.post('/', async function(req, res){
        try {
            const jsonBody = req.body;
            // console.log("jsonBody=" + JSON.stringify(jsonBody));
            // data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA...

            // console.log("jsonBody.data=" + jsonBody.data);

            let matches = jsonBody.data.match(/^data:.+\/(.+);base64,(.*)$/);
            let ext = matches[1];
            let base64_data = matches[2];
            let buffer = new Buffer(base64_data, 'base64');

            let sha1_buffer = sha1(buffer);
            // console.log("sha1_buffer=" + sha1_buffer);

            // let dirPath = process.env.IMAGE_ROOT_PATH + '/' + sha1_buffer.match(/.{1,4}/g).join('/');
            // console.log("dirPath=" + dirPath);
            //
            // if (!fs.existsSync(dirPath)){
            //     await makeDir(dirPath);
            // }

            let filePath = process.env.IMAGE_ROOT_PATH + '/' + sha1_buffer;
            await writeFile(filePath, buffer);

            res.json({status: 'success', message: 'OK', data: sha1_buffer});
        } catch (e) {
            res.json({status: 'error', message: e.message, data: e});
        }
    });

    app.use('/', router);

    app.listen(port);
    console.log('serverStart on port ' + port);
};

serverStart();